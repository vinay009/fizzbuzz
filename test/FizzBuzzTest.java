import static org.junit.Assert.*;

import org.junit.Test;

public class FizzBuzzTest {

	@Test
	public void testDivisibleBy3() {
		FizzBuzz b =  new FizzBuzz();
		String result = b.Buzzz(3);
		assertEquals("fizz", result);
		
	}
	//R2: If number is divisible by 5, return "buzz"
		@Test
		public void testDivisibleBy5() {
			FizzBuzz b = new FizzBuzz();
			// choose any number that's divisible by 5
			String result = b.Buzzz(5);
			assertEquals("buzz", result);
		}
		//R3: If number is divisible by 3 & 5, return "fizzbuzz"
			@Test
			public void testDivisibleBy3and5() {
				FizzBuzz b = new FizzBuzz();
				// choose any number that's divisible by 5
				String result = b.Buzzz(15);
				assertEquals("fizzbuzz", result);
			}
			// R4: If no other requirement fulfilled, return the number
				@Test
				public void testOtherNumber() {
				FizzBuzz b = new FizzBuzz();
				// choose any number that's divisible by 5
				String result = b.Buzzz(4);
				assertEquals("4", result);
				}
}
